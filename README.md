# HyperPubee Backend

A small layer on top of hyperpubee to facilitate persistence.
It is built around a simple database which keeps track of your published works.

Warning: this library is still in alfa, and there are likely to be breaking changes before it reaches v1.0.0.


## Install
`npm add hyperpubee-backend`

## Usage
There are two relevant config variables:
- dbConnectionStr
- corestoreLoc

dbConnectionStr defines the location of the sqlite db which stores the hypercore metadata.

corestoreLoc defines the location of the corestore which stores the actual hypercores.

The defaults are:
- dbConnectionStr: ./hyperpubee.db,
- corestoreLoc: ./my-hyperpubees

See https://www.npmjs.com/package/rc?activeTab=readme for how to pass config values. The app name is 'hyperpubee'
