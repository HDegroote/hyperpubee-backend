const path = require('node:path')

const sqlite3 = require('better-sqlite3')
const Corestore = require('corestore')
const { api, utils } = require('hyperpubee')

const HyperInterface = require('hyperpubee-hyper-interface')
const DbInterface = require('corestore-metadata-db')

class DbApp {
  constructor ({ corestoreLocation, hyperInterface, dbInterface }) {
    this.corestoreLocation = corestoreLocation
    this.hyperInterface = hyperInterface
    this.dbInterface = dbInterface
  }

  async createWork (text, name, author) {
    const work = await api.buildWorkFromText(
      text,
      name,
      this.hyperInterface,
      author
    )
    await work.ensureIsValid()

    this.dbInterface.addCore({
      name,
      key: work.key,
      corestorePath: this.corestoreLocation
    })
    return work
  }

  async loadStoredWorksInCorestore () {
    const works = this.dbInterface.getCoresOfCorestore(this.corestoreLocation)

    const res = []
    for (const dbWork of works) {
      const pubee = await api.readWork(dbWork.key, this.hyperInterface)
      await pubee.ensureIsValid()

      console.log(
        'Loaded and validated work',
        dbWork.name,
        `(${utils.getKeyAsStr(dbWork.key)})`
      )

      res.push(pubee)
    }

    return res
  }

  async close () {
    await this.hyperInterface.close()
  }

  static async initFromConfig (config) {
    const corestore = new Corestore(config.corestoreLoc)
    await corestore.ready()

    const corestoreLocation = path.resolve(config.corestoreLoc)

    const db = sqlite3(config.dbConnectionStr)

    return new DbApp({
      corestoreLocation,
      hyperInterface: new HyperInterface(corestore),
      dbInterface: new DbInterface(db)
    })
  }
}

module.exports = DbApp
