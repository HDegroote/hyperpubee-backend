const rc = require('rc')

const APPLICATION_NAME = 'hyperpubee'

function loadConfig () {
  const config = rc(APPLICATION_NAME, {
    dbConnectionStr: './hyperpubee.db',
    corestoreLoc: './my-hyperpubees'
  })

  return config
}

module.exports = loadConfig
