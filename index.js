const loadConfig = require('./lib/config')
const DbApp = require('./lib/db-app')

module.exports = {
  loadConfig,
  DbApp
}
