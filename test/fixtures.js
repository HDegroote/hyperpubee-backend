const Corestore = require('corestore')
const ram = require('random-access-memory')
const HyperInterface = require('hyperpubee-hyper-interface')
const DbInterface = require('corestore-metadata-db')

async function hyperInterfaceFactory () {
  const corestore = new Corestore(ram)
  await corestore.ready()

  const hyperInterface = new HyperInterface(corestore)
  return hyperInterface
}

function dbInterfaceFactory () {
  return DbInterface.initFromConnectionStr(':memory:')
}

module.exports = {
  hyperInterfaceFactory,
  dbInterfaceFactory
}
