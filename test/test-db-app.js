const path = require('node:path')
const { expect } = require('chai')
const temp = require('temp')
const fs = require('fs').promises
const { utils, constants: c } = require('hyperpubee')

const DbApp = require('../lib/db-app')
const fixtures = require('./fixtures')

temp.track() // Remove temp files after use

describe('Db-app tests', function () {
  let app, poemTxt

  this.beforeEach(async function () {
    app = new DbApp({
      corestoreLocation: 'dummy',
      hyperInterface: await fixtures.hyperInterfaceFactory(),
      dbInterface: fixtures.dbInterfaceFactory()
    })

    poemTxt = '[poetry]Line1\nLine2'
  })

  this.afterEach(async function () {
    await app.close()
  })

  describe('Test basic work creation', function () {
    let work

    this.beforeEach(async function () {
      work = await app.createWork(poemTxt, 'test work')
    })

    it('Can create a work', async function () {
      // Correct in db
      const lookedUpWork = app.dbInterface.getCoreByKey(work.key)
      expect(utils.areEqualKeys(lookedUpWork.key, work.key)).to.equal(true)

      // Correct in storage
      const core = await app.hyperInterface.readHypercore(work.key)
      expect(core.writable).to.equal(true)
    })

    it('Can load the works stored in a corestore', async function () {
      const work2 = await app.createWork('[poetry]LineX\nLineY', 'test work 2')

      const works = await app.loadStoredWorksInCorestore()

      const loadedKeys = Array.from(works.map((pubee) => utils.getKeyAsStr(pubee.key)))

      expect(loadedKeys.length).to.equal(2)
      expect(loadedKeys).to.have.members([
        utils.getKeyAsStr(work.key),
        utils.getKeyAsStr(work2.key)
      ])
    })

    it('Can init from a config', async function () {
      const dirPath = await temp.mkdir('configtests')

      const storeLoc = path.resolve(dirPath, 'testCorestore')
      const config = { corestoreLoc: storeLoc, dbConnectionStr: ':memory:' }

      try {
        const app = await DbApp.initFromConfig(config)

        expect(app.corestoreLocation).to.equal(storeLoc)
        await fs.access(storeLoc) // Ensure it exists (throws if not)

        // Ensure everything works
        await app.createWork('[poetry]line1\nLine2', Math.random().toString())
      } finally {
        await Promise.all([
          app.close(),
          temp.cleanup()
        ])
      }
    })
  })

  it('Can create a work with an author', async function () {
    const work = await app.createWork(poemTxt, 'test work', 'FitzG')

    // Correct in db
    const lookedUpWork = app.dbInterface.getCoreByKey(work.key)
    expect(utils.areEqualKeys(lookedUpWork.key, work.key)).to.equal(true)

    // Correct in storage
    const bee = await app.hyperInterface.readHyperbee(work.key)
    const { value: metadata } = await bee.get(c.METADATA)

    expect(metadata[c.METADATA_AUTHOR]).to.equal('FitzG')
  })
})
