const { expect } = require('chai')

const index = require('../index')

describe('Test exported interface', function () {
  it('Index contains the expected keys', function () {
    const expectedKeys = ['loadConfig', 'DbApp'].sort()
    expect(Object.keys(index).sort()).to.deep.equal(expectedKeys)
  })
})
