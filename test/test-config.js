const { expect } = require('chai')
const loadConfig = require('../lib/config')

describe('Test config', function () {
  it('Default config works', function () {
    const actualConfig = loadConfig()

    expect(actualConfig.dbConnectionStr).to.equal('./hyperpubee.db')
    expect(actualConfig.corestoreLoc).to.equal('./my-hyperpubees')
  })
})
